#! /usr/bin/env bash

# Exit on error
set -e

# Download signal
wget -O- https://updates.signal.org/desktop/apt/keys.asc | gpg --dearmor > signal-desktop-keyring.gpg
sudo bash -c "
cat signal-desktop-keyring.gpg | tee -a /usr/share/keyrings/signal-desktop-keyring.gpg > /dev/null
echo 'deb [arch=amd64 signed-by=/usr/share/keyrings/signal-desktop-keyring.gpg] tor+https://updates.signal.org/desktop/apt xenial main' |\
tee /etc/apt/sources.list.d/signal-xenial.list
apt update
apt install -y dialog
"

#Create a temporary file to store dialog result
tmp_file=$(tempfile 2>/dev/null) || tmp_file=/tmp/test$$
trap "rm -f $tmp_file" 0 1 2 5 15

# Menu for Signal multiple instances
dialog --menu "Vous pouvez avoir plusieurs instances de Signal sur votre ordinateur afin d'avoir plusieurs numéros en même temps. Ce ne prendra pas plus d'espace disque, et la mise à jour de l'un entrainera celle des autres. Combien d'instances Signal souhaitez vous installer ?"  20 60 12 1 "UNE instance Signal" 2 "DEUX instances Signal" 3 "TROIS instances Signal" 4 "QUATRE instances Signal" 2>$tmp_file
result=$(cat $tmp_file)

apt download signal-desktop
mkdir -p ~/Applications/signal-desktop
dpkg-deb -xv $(ls signal-desktop*.deb) ~/Applications/signal-desktop

# Create config directories
mkdir -p /live/persistence/TailsData_unlocked/dotfiles/.config/
mkdir -p /live/persistence/TailsData_unlocked/dotfiles/.local/share/applications

# Download Signal-template
template=/tmp/template.desktop
wget -nv https://0xacab.org/jc7v/signal-sous-tails/-/raw/master/code/launchers/Signal-template.desktop?inline=false -O $template

# Install as desktop icons as necessary
for ((i=1;i<=$result;i++ ))
do
    export SIGNALID=$i
    envsubst < $template > /home/amnesia/.local/share/applications/Signal-$i.desktop

done


#wget https://0xacab.org/jc7v/signal-sous-tails/-/raw/master/code/launchers/Signal.desktop?inline=false -O /home/amnesia/.local/share/applications/Signal.desktop


cp -r ~/Applications/ /live/persistence/TailsData_unlocked/dotfiles/ ||:
cp ~/.local/share/applications/*.desktop /live/persistence/TailsData_unlocked/dotfiles/.local/share/applications/ ||:

update-desktop-database /home/amnesia/.local/share/applications/

echo "Signal Desktop a été installé!"

